// Dear emacs, this is -*- c++ -*-
#ifndef TRTFramework_CutFlow_h
#define TRTFramework_CutFlow_h

// System include(s):
#include <string>
#include <vector>
#include <map>
#include <iosfwd>

namespace xTRT {

  /// @class CutFlow
  /// @brief Class to hold the amount of passing events/objects given a selection
  ///
  /// Object holding event/object cutflow information
  ///
  /// This is an extremely simple class, used by the code for trivial
  /// cross-checks with the other analysis frameworks.
  ///
  /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
  ///
  class CutFlow {

  public:
    /// Helper typedef
    typedef std::vector< std::pair< std::string, unsigned int > > initcuts_t;

    /// Constructor with a name
    CutFlow( const std::string& name = "CutFlow",
             const initcuts_t& initialCuts = {} );

    /// Copy constructor
    CutFlow( const CutFlow& parent ) = default;
    /// Assignment operator
    CutFlow& operator= ( const CutFlow& rhs ) = default;

    /// Add a named step in the cutflow
    std::size_t addStep( const std::string& name );

    /// Increment the cutflow for a given step by one
    unsigned int incrementStep( std::size_t step );
    /// Increment the cutflow for a given step by the specified amount
    unsigned int incrementStep( std::size_t step, unsigned int amount );

    /// Set the name of this cutflow object
    void setName( const std::string& name );
    /// Get the name of this cutflow object
    const std::string& name() const;

    /// Get the number of steps in this cutflow
    std::size_t numberOfSteps() const;

    /// Get the step index for a given name
    std::size_t stepIndex( const std::string& name ) const;
    /// Get the step name for a given index
    const std::string& stepName( std::size_t step ) const;

    /// Get the value of a given step in the cutflow
    unsigned int stepValue( const std::size_t step ) const;
    /// Get the value of a given step in the cutflow
    unsigned int stepValue( const std::string& name ) const;

  private:
    /// Function checking that a given step index is valid for this cutflow
    void checkStepIsValid( std::size_t step ) const;

    /// The name of this cutflow
    std::string m_name;
    /// Vector holding the cutflow counts
    std::vector< unsigned int > m_cutflow;
    /// Vector holding the names of the cutflow steps
    std::vector< std::string > m_stepNames;

  }; // class CutFlow

} // namespace

namespace std {

  /// Print operator for a cutflow object
  std::ostream& operator<< ( std::ostream& out, const xTRT::CutFlow& cf );

} //namespace

#endif
